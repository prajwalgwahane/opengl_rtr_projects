#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <gl/gl.h>
#include <gl/glu.h>

//Macros Declaration
#define WinWidth 800
#define WinHeight 600
#define pi 3.14
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Global Variable declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
GLfloat fAngle = 0, x, y, i = 0;



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	void Initialize(void);
	void Display(void);
	void Update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("My Window");
	int iXaxis, iYaxis;
	int iResultX, iResultY;
	RECT rcVar;
	bool bDone = false;

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcVar, 0);
	iXaxis = rcVar.right;
	iYaxis = rcVar.bottom;
	iResultX = ((rcVar.right) / 2) - 400; //window width - 400
	iResultY = ((rcVar.bottom) / 2) - 300; //window height - 300

	//code to calculate centering of X and Y axis
	if (fopen_s(&gpFile, "PGWlog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Desired file does not exist..."), TEXT("Error"), MB_ICONERROR | MB_OK);

		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created\n And Program Started Successfully!!! \n");
	}

	//code
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL Blue Screen"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iResultX,
		iResultY,
		WinWidth,
		WinHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	//Game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				
				//Here you should call display() for OpenGL Rendering
				Display();

				//Here you should call update() for OpenGL Rendering
				Update();

			}
		}
	}
	return(msg.wParam);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local variable declaration
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	//TCHAR str[] = TEXT("HELLO WORLD");

	//function declaration
	void ToggleFullScreen(void); //signature or prototype
	void Resize(int, int);
	void Uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_PAINT:
		GetClientRect(hwnd, &rc);
		hdc = BeginPaint(hwnd, &ps);
		//hdc = GetDC(hwnd);
		SetBkColor(hdc, RGB(0, 0, 0));
		SetTextColor(hdc, RGB(0, 255, 0));
		//DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		EndPaint(hwnd, &ps);
		//ReleaseDC(hwnd, hdc);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	//local variable declaration
	MONITORINFO mi = { sizeof(MONITORINFO) };


	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd,
					GWL_STYLE,
					dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd,
			GWL_STYLE,
			dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}
void Initialize(void)
{
	//function declaration
	void Resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "Choose PixelFormat() Fail!!!\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Fail!!!\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Fail!!!\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Fail!!!\n");
		DestroyWindow(ghwnd);
	}

	//SetClearColor
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//warm up Resize call
	Resize(WinWidth, WinHeight);
}
void Resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}
void Display(void)
{
	
	//code
	const int CirclePoints = 1000; 
	static GLfloat radius = 1.0f, Radius = 0.18f;
	static GLfloat radius1 = 0.18f, Radius1 = 1.0f;

	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluLookAt(0.0f, 0.0f, 50.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0f, 0.0f, -6.0f);
	glLineWidth(1.0);
	//glPushMatrix();

	glRotatef(fAngle, 0.0f, 0.0f, 1.0f);
	glPushMatrix();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))+0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))-0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))+0.8f, 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))-0.8f, 0.0f);	
	}

	glEnd();

	glPopMatrix();
	//glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluLookAt(0.0f, 0.0f, 50.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(11.25f, 0.0f, 0.0f, 1.0f);
	glLineWidth(1.0);
	//glPushMatrix();

	glRotatef(fAngle, 0.0f, 0.0f, 1.0f);
	glPushMatrix();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))+0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))-0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))+0.8f, 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))-0.8f, 0.0f);	
	}

	glEnd();

	glPopMatrix();


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluLookAt(0.0f, 0.0f, 50.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(22.5f, 0.0f, 0.0f, 1.0f);
	glLineWidth(1.0);
	//glPushMatrix();

	glRotatef(fAngle, 0.0f, 0.0f, 1.0f);
	glPushMatrix();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))+0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))-0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))+0.8f, 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))-0.8f, 0.0f);	
	}

	glEnd();

	glPopMatrix();

	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluLookAt(0.0f, 0.0f, 50.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(33.75f, 0.0f, 0.0f, 1.0f);
	glLineWidth(1.0);
	//glPushMatrix();

	glRotatef(fAngle, 0.0f, 0.0f, 1.0f);
	glPushMatrix();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))+0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))-0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))+0.8f, 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))-0.8f, 0.0f);	
	}

	glEnd();

	glPopMatrix();


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluLookAt(0.0f, 0.0f, 50.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(45.0f, 0.0f, 0.0f, 1.0f);
	glLineWidth(1.0);
	//glPushMatrix();

	glRotatef(fAngle, 0.0f, 0.0f, 1.0f);
	glPushMatrix();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))+0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))-0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))+0.8f, 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))-0.8f, 0.0f);	
	}

	glEnd();

	glPopMatrix();


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluLookAt(0.0f, 0.0f, 50.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(56.25f, 0.0f, 0.0f, 1.0f);
	glLineWidth(1.0);
	//glPushMatrix();

	glRotatef(fAngle, 0.0f, 0.0f, 1.0f);
	glPushMatrix();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))+0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))-0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))+0.8f, 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))-0.8f, 0.0f);	
	}

	glEnd();
	glPopMatrix();

	

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluLookAt(0.0f, 0.0f, 50.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(67.5f, 0.0f, 0.0f, 1.0f);
	glLineWidth(1.0);
	//glPushMatrix();

	glRotatef(fAngle, 0.0f, 0.0f, 1.0f);
	glPushMatrix();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))+0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))-0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))+0.8f, 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))-0.8f, 0.0f);	
	}

	glEnd();
	glPopMatrix();

	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluLookAt(0.0f, 0.0f, 50.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(78.75f, 0.0f, 0.0f, 1.0f);
	glLineWidth(1.0);
	//glPushMatrix();

	glRotatef(fAngle, 0.0f, 0.0f, 1.0f);
	glPushMatrix();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))+0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f((y + (radius * cos(2 * pi * i / CirclePoints)))-0.8f, y + (Radius * sin(2 * pi * i / CirclePoints)), 0.0f);	
	}

	glEnd();

	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))+0.8f, 0.0f);	
	}

	glEnd();
	
	glBegin(GL_LINE_STRIP);
		
	for(i = 0; i <= CirclePoints ; i++) { 
			
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(y + (radius1 * cos(2 * pi * i / CirclePoints)), (y + (Radius1 * sin(2 * pi * i / CirclePoints)))-0.8f, 0.0f);	
	}

	glEnd();

	glPopMatrix();

	SwapBuffers(ghdc);

}
void Update(void)
{
	fAngle = fAngle + 0.11f;
	if (fAngle >= 90.0f)
		fAngle = 0.0f;

}
void Uninitialize(void)
{
	//code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd,
			GWL_STYLE,
			dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);

	}


	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Closed Successfully\n Program complete successfully!!!");
		fclose(gpFile);
		gpFile = NULL;
	}
}
